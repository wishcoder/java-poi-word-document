/**
 * 
 */
package com.wishcoder.poi.docx;

import java.util.List;

/**
 * @author ajaysingh
 *
 */
public class EmployeeData {

	private final String id;
	private final String name;
	private final int regularHours;
	private final SiteVisited siteVisited;
	
	/**
	 * @param id
	 * @param name
	 * @param regularHours
	 * @param siteVisited
	 */
	public EmployeeData(String id, String name, int regularHours, SiteVisited siteVisited) {
		super();
		this.id = id;
		this.name = name;
		this.regularHours = regularHours;
		this.siteVisited = siteVisited;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the regularHours
	 */
	public int getRegularHours() {
		return regularHours;
	}

	/**
	 * @return the siteVisited
	 */
	public SiteVisited getSiteVisited() {
		return siteVisited;
	}

	/**
	 * Return total of hours
	 * 
	 * @param employeeList
	 * @return
	 */
	public static int getTotalHours(List<EmployeeData> employeeList){
		int tolatHours = 0;
		
		for(EmployeeData employeeData : employeeList){
			tolatHours += employeeData.getRegularHours();
		}
		
		return tolatHours;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EmployeeData [id=" + id + ", name=" + name + ", regularHours=" + regularHours + ", siteVisited="
				+ siteVisited + "]";
	}

	/**
	 * SiteVisited class
	 * @author ajaysingh
	 *
	 */
	public static class SiteVisited{
		private final String siteName;
		private final String siteUrl;
		
		/**
		 * @param siteName
		 * @param siteUrl
		 */
		public SiteVisited(String siteName, String siteUrl) {
			super();
			this.siteName = siteName;
			this.siteUrl = siteUrl;
		}
	
		public String getSiteName() {
			return siteName;
		}
		
		public String getSiteUrl() {
			return siteUrl;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "SiteVisited [siteName=" + siteName + ", siteUrl=" + siteUrl + "]";
		}
	}
	
}
